package Util;

import java.util.ArrayList;
import java.util.Collections;

import model.*;

/**
 * Classe utilitaire.
 * 
 * @author Martin
 *
 */
public class TileUtil {
	
	public static final int MAX_TILE = 136;
	public static final int NB_OCCURRENCE = 4;
	public static final int NB_MAX = 9;
	
	public static final String FAMILY[] = {"bamboo","character","circle"};
	public static final char COLOR[] = {'R','G','W'};
	public static final char DIRECTION[] = {'N','E','S','W'};
	
	public static final String BACK_TILE = "images/tile/back/face-down.png"; // 14 px de decal pour superposer 2 tuiles pour le mur
	
	public static final int MARGIN = 38; // l'entete de fenetre fait exactement 38 px
	
	public static int tileSizeHand = 96; 
	public static int tileSizeVTP = 64; 
	public static int tileSizeDP = 64;
	public static int tileSizeWall = 32; 
	
	
	/**
	 * Utilitaire pour melanger le deck.
	 * 
	 * @param tuiles tuiles
	 * @return le deck mélangé.
	 */
	public static ArrayList<Tile> shuffle(ArrayList<Tile> tuiles){
		Collections.shuffle(tuiles);
		return tuiles;
	}

	
	/**
	 * Génère un nombre aléatoire [0,nbMax]
	 * 
	 * @param nbMax nombre maximum
	 * @return un nombre [0,nbMax]
	 */
	public static int random(int nbMax){
		return (((int)(Math.random()*1000))%(nbMax+1));
	}
	
	/**
	 * On initialise le deck de 136 tuiles.
	 * 
	 * @return le deck construit, non-mélangé.
	 */
	public static ArrayList<Tile> initDeck(){
		ArrayList<Tile> deck = new ArrayList<Tile>();
		String img = "images/tile/";
		

		//0-107 : chiffres
		for(int i = 0; i < FAMILY.length;i++){
			for(int j = 1 ; j <= NB_MAX ; j++){
				for(int k = 0; k < NB_OCCURRENCE;k++){
					deck.add(new Figure(j,FAMILY[i],img + "figure/" + FAMILY[i] + "/" + j + ".png"));
				}
			}
		}

		//107-123 : vents
		for(int i = 0; i < DIRECTION.length; i++){
			for(int j = 0; j < NB_OCCURRENCE; j++){
				deck.add(new Wind(DIRECTION[i],img + "wind/" + DIRECTION[i] + ".png"));
			}
		}
		
		//123-135 : dragons
		for(int i = 0; i < COLOR.length; i++){
			for(int j = 0; j < NB_OCCURRENCE; j++){
				deck.add(new Dragon(COLOR[i],img + "dragon/" + COLOR[i] + ".png"));
			}
		}		
		
		return deck;
	}
	
	/**
	 *  Détermine si trois tuiles forment une suite.
	 * 
	 * @param t1 tuile 1
	 * @param t2 tuile 2
	 * @param t3 tuile 3
	 * @return true si les 3 tuiles forment une suite.
	 */
	public static boolean isSuite(Tile t1, Tile t2, Tile t3){
		if((!isFigure(t1)) || (!isFigure(t2)) || (!isFigure(t3))) return false; // si c'est des dragons ou des vents -> pas de suite possible
		if((! ((Figure)t1).isSameFamily(((Figure)t2))) || (! ((Figure)t1).isSameFamily(((Figure)t3)))) return false;// Si pas de la même famille -> pas de suite possible

		
		if(((Figure)t2).getValue() == ((Figure)t1).getValue()+1){ // t1 < t2
			if(((Figure)t3).getValue() == ((Figure)t2).getValue()+1 || // t1 < t2 < t3
					((Figure)t1).getValue() == ((Figure)t3).getValue()+1) return true; // t3 < t1 < t2
		}
		else if(((Figure)t1).getValue() == ((Figure)t2).getValue()+1){ // t2 < t1
			if(((Figure)t3).getValue() == ((Figure)t1).getValue()+1 || // t2 < t1 < t3
					((Figure)t2).getValue() == ((Figure)t3).getValue()+1) return true; // t3 < t2 < t1
		}
		else if(((Figure)t3).getValue() == ((Figure)t1).getValue()+1 && ((Figure)t2).getValue() == ((Figure)t3).getValue()+1) return true; // t1 < t3 < t2
		else if(((Figure)t1).getValue() == ((Figure)t3).getValue()+1 && ((Figure)t3).getValue() == ((Figure)t2).getValue()+1) return true;//t2 < t3 < t1
		
		return false;
	}
	
	
	/**
	 * Détermine si toutes les tuiles d'une main sont unique.
	 * 
	 * @param combinations liste de combinaison de 3 tuiles.
	 * @return true si aucune tuile n'est en double.
	 */
	public static boolean everyTileUnique(ArrayList<Tile[]> combinations){
		// je remet tout sur un seul ArrayList pour plus de facilités
		ArrayList<Tile> temp = new ArrayList<Tile>();
		for(int i = 0; i < combinations.size() ; i++){
			for(int j = 0; j < combinations.get(i).length ; j++){
				temp.add(combinations.get(i)[j]);
			}
		}
		
		// des que je trouve 2 tuiles avec une id identique je renvoie false
		for(int i = 0; i < temp.size()-1 ; i++){
			for(int j = i+1 ; j < temp.size() ; j++){
				if(temp.get(i).equals(temp.get(j))) return false;
			}
		}
		return true;
	}
	
	/**
	 * Détermine si une tuile est une Figure
	 * 
	 * @param t une tuile
	 * @return true si la tuile est une Figure
	 */
	public static boolean isFigure(Tile t){
		return(t.getClass() != Dragon.class && t.getClass() != Wind.class);
	}
	
	/**
	 * Met l'objet en pause pendant un temps déterminé
	 * 
	 * @param obj l'objet à mettre en pause.
	 * @param sec le nombre de seconde durant laquelle il faut mettre l'objet en pause.
	 */
	public static void Timer(Object obj, double sec){
		synchronized(obj) { 
			try {
				obj.wait((long)sec*1000); //on attend que le joueur défausse une tuile
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}
	}
	
	/**
	 *  Renvoit une combinaison sous forme d'une ligne pour l'affichage
	 * 
	 * @param tiles combinaison de 3 tuiles
	 * @return ligne de texte contenant la combinaison
	 */
	public static String combinationToString(Tile[] tiles){
		String goodString = "";
		if(tiles[0].getClass() != Figure.class) goodString = "3x " + tiles[0];
		else{
			if(((Figure)tiles[0]).sameValue(tiles[1])) goodString = "3x " + tiles[0];
			else goodString = ((Figure)tiles[2]).getValue() + "," + ((Figure)tiles[0]).getValue() + "," + ((Figure)tiles[1]).getValue() + " de " + ((Figure)tiles[0]).getFamille();
		}
		return goodString;
	}
	
	/**
	 *  Renvoit une liste de tuile sous forme d'une ligne formatée pour le transfert d'information.
	 * 
	 * @param tiles liste de tuile à envoyer.
	 * @return ligne formatée contenant la liste de tuile.
	 */
	public static String arrayOfTileToString(ArrayList<Tile> tiles){
		String megaString = "" + tiles.get(0).getId();
		for(int i = 1; i < tiles.size() ; i++){
			megaString += ":" + tiles.get(i).getId();
		}
		return megaString;
	}
	
	/**
	 *  renvoit une combinaison de tuile sous forme d'une ligne formatée pour le transfert d'information.
	 * 
	 * @param tiles combinaison de tuile à envoyer.
	 * @return ligne formatée contenant la combinaison de tuile.
	 */
	public static String arrayOfTileToString(Tile[] tiles){
		String megaString = "" + tiles[0].getId();
		for(int i = 1; i < tiles.length ; i++){
			megaString += ":" + tiles[i].getId();
		}
		return megaString;
	}
	
	/**
	 * Recré une liste de tuile grâce au deck de base et une liste de tuile sous forme d'une ligne formatée.
	 * 
	 * @param megaString liste de tuile sous forme d'une ligne formatée
	 * @param deck liste de tuile contenant toutes les tuiles du jeu.
	 * @return une liste de tuile.
	 */
	public static ArrayList<Tile> stringToArray(String megaString, ArrayList<Tile> deck){
		String ids[] = megaString.split(":");
		ArrayList<Tile> tiles = new ArrayList<Tile>();
		for(int i = 0; i < ids.length ; i++){
			tiles.add(deck.get(Integer.parseInt(ids[i])));
		}
		return tiles;
	}
	
	/**
	 * Recré une combinaison de tuile grâce au deck de base et une liste de tuile sous forme d'une ligne formatée.
	 * 
	 * @param megaString liste de tuile sous forme d'une ligne formatée
	 * @param deck liste de tuile contenant toutes les tuiles du jeu.
	 * @return une combinaison de tuile.
	 */
	public static Tile[] stringToCombination(String megaString, ArrayList<Tile> deck){
		String ids[] = megaString.split(":");
		Tile[] combination = new Tile[3];
		for(int i = 0; i < ids.length ; i++){
			combination[i] = (deck.get(Integer.parseInt(ids[i])));
		}
		return combination;
	}
	
	/**
	 * Recré une tuile à partir d'une ligne formatée représentant une tuile et du deck de base.
	 * 
	 * @param s une ligne formatée représentant une tuile.
	 * @param deck liste de tuile contenant toutes les tuiles du jeu.
	 * @return une tuile.
	 */
	public static Tile stringToTile(String s, ArrayList<Tile> deck){
		String id = s.substring(1);
		return(deck.get(Integer.parseInt(id)));
	}
}
