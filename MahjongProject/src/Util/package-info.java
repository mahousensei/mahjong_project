/**
 * Ce package contient toutes les classes utilitaires :
 * <ul>
 * <li>La classe des utilitaires pour les images <code>ImageTool.java</code></li>
 * <li>La classe des utilitaires pour les tuiles <code>TileUtil.java</code></li>
 * </ul>
 * 
 * @author Dylan Brossel
 */
package Util;