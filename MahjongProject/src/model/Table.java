package model;

import java.util.ArrayList;

/**
 * La table, contenant le mur notamment.
 * 
 * @author Martin
 *
 */
public class Table {
	private ArrayList<Tile> wall = new ArrayList<Tile>();
	private Tile lastTile;
	private int idCurrTile = -1;
	
	public Table() {
	}

	public ArrayList<Tile> getWall() {
		return wall;
	}

	public void setWall(ArrayList<Tile> wall) {
		this.wall = new ArrayList<Tile>(wall);
	}

	public Tile getLastTile() {
		return lastTile;
	}

	public void setLastTile(Tile lastTile) {
		this.lastTile = lastTile;
	}

	public int getIdCurrTile() {
		return idCurrTile;
	}

	public void setIdCurrTile(int idCurrTile) {
		this.idCurrTile = idCurrTile;
	}

}
