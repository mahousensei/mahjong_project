package model;

/**
 * Les dragons.
 * 
 * @author Martin
 *
 */
public class Dragon extends Honor{
	private char color;

	public Dragon(char color, String image) {
		super(image);
		this.setColor(color);
	}

	public char getColor() {
		return color;
	}

	public void setColor(char color) {
		this.color = color;
	}

}
