package model;

import java.util.ArrayList;

import Util.TileUtil;

/**
 * Le joueur, sa main et ses défausses.
 * 
 * @author Martin
 *
 */
public class Player {
	private ArrayList<Tile> hand = new ArrayList<Tile>();
	private ArrayList<Tile> discard = new ArrayList<Tile>();
	private String pseudo = "Dummy";
	private int buttonPressed = -1;
	
	public Player(String pseudo) {
		this.pseudo = pseudo;
	}
	
	/**
	 * Décide si le joueur peut pon.
	 * 
	 * @param tile la dernière tuile défaussée.
	 * @return true si le joueur peut pon.
	 */
	public boolean canPon(Tile tile){
		int nb = 0;
		for(int i = 0; i< hand.size(); i++){
			if(hand.get(i).sameValue(tile) == true){
				if(hand.get(i).isLock() == false) nb++;
			}
		}
		return(nb >= 2);
	}
	
	/**
	 * Décide si le joueur peut chi
	 * 
	 * @param tile la dernière tuile défaussée.
	 * @return true si le joueur peut chi
	 */
	public boolean canChi(Tile tile){
		if(tile.getClass() != Figure.class) return false;
		for(int i= 0; i < hand.size()-1 ; i++){
			for(int j = i+1; j < hand.size(); j++){
				if(hand.get(i).isLock() == false && hand.get(j).isLock() == false) {
					if(TileUtil.isSuite(hand.get(i), hand.get(j), tile)) return true;
				}
			}
		}
		return false;
	}

	
	/**
	 * Décide si le joueur peut ron
	 * 
	 * @param tile la dernière tuile défaussée.
	 * @return true si le joueur peut ron.
	 */
	public boolean canRon(Tile tile){
		ArrayList<Tile> copy = new ArrayList<Tile>(getHand());
		if(tile != null) copy.add(tile);
		ArrayList<Tile[]> pairs = new ArrayList<Tile[]>();
		//On cherche toutes les tuiles pairs.
		
		for(int i = 0; i < copy.size()-1 ; i++){
			boolean match = false;
			for(int j = i+1; j < copy.size();j++){
				if(copy.get(i).sameValue(copy.get(j))){
					pairs.add( new Tile[] {copy.get(i), copy.get(j)});
					copy.remove(j);
					copy.remove(i);
					match = true;
				}
			}
			if(match) i--;
		}
		
		// premières conditions de victoire et de défaite
		if(pairs.size() == 7) return true; // 7 pairs donc auto win
		if(pairs.size() == 0) return false; // même pas une pair
		
		for(int i = 0 ; i < pairs.size();i++){
			ArrayList<Tile> temp = new ArrayList<Tile>(getHand());
			if(tile != null) temp.add(tile);
			ArrayList<Tile[]> combinations = new ArrayList<Tile[]>();
			// on prend la main moins une paire.
			temp.remove(pairs.get(i)[0]);
			temp.remove(pairs.get(i)[1]);
			
			combinations = new ArrayList<Tile[]>(getCombinations(temp,2));
			
			// On essaye toutes les combinaisons entre-elle pour essayer d'en trouver une sans tuile en double
			if(combinations.size() >= 4){
				for(int j = 0 ; j < combinations.size()-3 ; j++){
					for(int k = j+1 ; k < combinations.size()-2 ; k++){
						for(int l = k+1 ; l < combinations.size()-1 ; l++){
							for(int m = l+1 ; m < combinations.size();m++){
								ArrayList<Tile[]> temp2 = new ArrayList<Tile[]>();
								temp2.add(combinations.get(j));
								temp2.add(combinations.get(k));
								temp2.add(combinations.get(l));
								temp2.add(combinations.get(m));
								if(TileUtil.everyTileUnique(temp2)) return true;
							}
						}
					}
				}
			}
		}
		return false;
	}
	
	/**
	 * Détermine toutes les combinaisons possibles d'un main. 
	 * 
	 * @param tiles liste de tuiles où l'on veut chercher les combinaisons.
	 * @param mode 0 : chi, 1 : pon, 2 chi and pon.
	 * @return liste de combinaisons.
	 */
	static public ArrayList<Tile[]> getCombinations(ArrayList<Tile> tiles, int mode){
		ArrayList<Tile[]> combinations = new ArrayList<Tile[]>();
		ArrayList<Tile> temp = new ArrayList<Tile>(tiles);
		
		for(int j = 0 ; j < temp.size()-2 ; j++){
			for(int k = j+1 ; k < temp.size()-1 ; k++){
				for(int l = k+1 ; l < temp.size() ; l++){
					if(mode == 2 || mode == 1){
						if( temp.get(j).sameValue(temp.get(k)) && temp.get(k).sameValue(temp.get(l))) combinations.add(new Tile[]{temp.get(j),temp.get(k),temp.get(l)});
					}
					if(mode == 2 || mode == 0){
						if(TileUtil.isSuite(temp.get(j),temp.get(k),temp.get(l))) combinations.add(new Tile[]{temp.get(j),temp.get(k),temp.get(l)});
					}
				}
			}
		}
		
		return combinations;
	}
	
	/**
	 * Détermine si le joueur à une main gagnante.
	 * 
	 * @return true si le joueur à gagné.
	 */
	public boolean isVictory(){
		return(canRon(null));
	}
	
	public ArrayList<Tile> getHand() {
		return hand;
	}
	public void setHand(ArrayList<Tile> hand) {
		this.hand = hand;
	}
	public ArrayList<Tile> getDiscard() {
		return discard;
	}
	public void setDiscard(ArrayList<Tile> discard) {
		this.discard = discard;
	}
	
	public String getPseudo() {
		return pseudo;
	}
	public void setPseudo(String pseudo) {
		this.pseudo = pseudo;
	}
	public int getButtonPressed() {
		return buttonPressed;
	}
	public void setButtonPressed(int buttonPressed) {
		this.buttonPressed = buttonPressed;
	}
}
