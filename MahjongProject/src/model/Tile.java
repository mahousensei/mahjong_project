package model;

/**
 *  Classe mère des tuiles.
 * 
 * @author Martin
 *
 */
public class Tile {
	private int id = 0; // 0 - 135
	private String image = "default.png";
	private static int i = 0;
	private boolean lock = false;

	public Tile() {
		id = i;
		i++;
	}
	public Tile(String image){
		this();
		this.setImage(image);
	}

	public int getId() {
		return id;
	}

	public String getImage() {
		return image;
	}

	public void setImage(String image) {
		this.image = image;
	}
	
	public boolean isLock() {
		return lock;
	}
	public void setLock(boolean lock) {
		this.lock = lock;
	}
	
	/**
	 * Compare deux tuiles au niveau de leur classe ainsi que leurs paramètres respectifs.
	 * 
	 * @param tile la tuile à comparer.
	 * @return true si les tuiles sont deux des 4 copies d'une même tuile.
	 */
	public boolean sameValue(Tile tile){
		if (getClass() != tile.getClass()) return false;
		if(getClass() == Dragon.class) if(((Dragon)this).getColor() == ((Dragon)tile).getColor()) return true;
		if(getClass() == Wind.class) if(((Wind)this).getDirection() == ((Wind)tile).getDirection()) return true;
		if (getClass() == Figure.class) if(((Figure)this).getFamily().equals(((Figure)tile).getFamily()) && ((Figure)this).getValue() == ((Figure)tile).getValue()) return true;
		return false;
	}
	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + id;
		return result;
	}
	
	// compare si 2 tuiles sont strictement identique -> meme id
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		if (getId() != ((Tile)obj).getId())
			return false;
		return true;
	}
	
	@Override
	public String toString() {
		if(this.getClass() == Dragon.class){
			switch(((Dragon)this).getColor()){
				case 'R' : return "dragon rouge";
				case 'G' : return "dragon vert";
				case 'W' : return "dragon blanc";
			}
		}
		if(this.getClass() == Wind.class){
			switch(((Wind)this).getDirection()){
				case 'N' : return "vent du nord";
				case 'E' : return "vent de l'est";
				case 'S' : return "vent du sud";
				case 'W' : return "vent de l'ouest";
			}
		}
		if(this.getClass() == Figure.class){
			String family = "";
			switch(((Figure)this).getFamily()){
				case "bamboo" : family = "bambou"; break;
				case "character" : family = "caractère"; break;
				case "circle" : family = "cercle"; break;
			}
			return ((Figure)this).getValue() + " de " + family;
		}
		return "Tile []";
	}
	
}
