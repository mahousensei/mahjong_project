package view;

import java.awt.BorderLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.Box;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;

import controller.MahjongController;
import network.Client;
import network.Host;

/**
 * Frame du lobby affichant les 4 joueurs et si on peut lancer la partie.
 * 
 * @author Martin
 *
 */
@SuppressWarnings("serial")
public class LobbyFrame extends JFrame implements ActionListener{
	JLabel[] tabLabel = new JLabel[4];
	JButton launchGame = new JButton("Lancer la partie");
	int nbPlayer = 0;
	char type;
	Host host;
	
	public LobbyFrame(String pseudo, String ip, char type) throws NumberFormatException, Exception {
		super("MahjongProject");
		this.type = type;
		
		this.getContentPane().setLayout(new BorderLayout());
		Box center = Box.createVerticalBox();
		for(int i = 0; i < 4 ; i++){
			tabLabel[i] = new JLabel("Player " + (i+1) + " : ");
			center.add(tabLabel[i]);
		}
		this.add(center,BorderLayout.CENTER);
		launchGame.addActionListener(this);
		launchGame.setEnabled(false);
		this.add(launchGame,BorderLayout.SOUTH);
		
		
		if(type == 'H'){ // si c'est l'host
			tabLabel[0].setText(tabLabel[0].getText() + pseudo);
			nbPlayer++;
			this.host = new Host(pseudo, Integer.parseInt(ip), this);
		}
		else{
			String[] ipPort = ip.split(":");
			new Client(pseudo,ipPort[0],Integer.parseInt(ipPort[1]), this);
		}
		
		
		this.setVisible(true);
		this.pack();
	}
	
	public void addPlayer(String pseudo){
		tabLabel[nbPlayer].setText(tabLabel[nbPlayer].getText() + pseudo);
		nbPlayer++;
		
		if(nbPlayer == 4 && type == 'H') launchGame.setEnabled(true);
	}
	
	public void launchGameClient(Client client){
		MahjongController mc = new MahjongController(client);
		client.setMc(mc);
		this.dispose();
	}
	
	
	@Override
	public void actionPerformed(ActionEvent e) {
		MahjongController mc = new MahjongController(host);
		host.setMc(mc);
		host.sendtoall("dispose lobby");
		this.dispose();
	}

}
