package view;

import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

import javax.swing.Box;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;

import Util.TileUtil;
import model.Tile;

/**
 * Frame d'aide à l'identification des tuiles et comment défausser une tuile.
 * 
 * @author Dylan
 *
 */
@SuppressWarnings("serial")
public class HelpFrame extends JFrame implements ActionListener{
	private JLabel[][] tabLabel = new JLabel[34][2];
	private JLabel clicDroit = new JLabel("Clic droit pour défausser une de vos tuile.");
	private JButton closeFrame = new JButton("Fermer");
	private ArrayList<Tile> deck = TileUtil.initDeck();
	private Box[] box = new Box[34];

	public HelpFrame() {
		super("Aide");
		
		this.getContentPane().setLayout(new FlowLayout());
		
		int i = 0;
		for(int j = 0; j<34; j++){
			box[j] = Box.createVerticalBox();
			tabLabel[j][0] = new JLabel( new ImageIcon(deck.get(i).getImage()));
			box[j].add(tabLabel[j][0]);
			tabLabel[j][1] = new JLabel(deck.get(i).toString());
			box[j].add(tabLabel[j][1]);	
			this.add(box[j]);
			i=i+4;
		}
		
		closeFrame.addActionListener(this);
		this.add(clicDroit);
		this.add(closeFrame);
		
		this.setSize(960, 810);
		this.setLocationRelativeTo(null);
		this.setDefaultCloseOperation(DISPOSE_ON_CLOSE);
		this.setVisible(true);
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		this.dispose();
	}

}
