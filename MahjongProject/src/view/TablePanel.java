package view;

import java.awt.Color;

import javax.swing.BorderFactory;
import javax.swing.JPanel;

import controller.MahjongController;
import Util.TileUtil;
import system.Game;

/**
 * JPanel contenant les panels de tout les joueurs.
 * 
 * @author Martin
 *
 */
@SuppressWarnings("serial")
public class TablePanel extends JPanel{
	private Game game;
	
	private PlayerPanel pp;
	private WallPanel wp;
	
	private OpponentPanel[] ops = new OpponentPanel[3];
	
	public TablePanel(MahjongController mc, int h, int w) {
		this.setGame(mc.getGame());
	    this.setLayout(null);
	    this.setBackground(new Color(31,126,58));
	    
		this.setBounds(0, 0, w, h);
		
		wp = new WallPanel(game.getTable(), w, h);
		this.add(wp);
		
		ops[0] = new OpponentPanel('W',w,h);
		ops[0].setBounds(0, 0, 256, h-256-TileUtil.MARGIN);
		ops[1] = new OpponentPanel('N',w,h);
		ops[1].setBounds(256, 0, w-256-256, 256);
		ops[2] = new OpponentPanel('E',w,h);
		ops[2].setBounds(w-256, 0, 256, h-256-TileUtil.MARGIN);

		for(int i = 0; i < ops.length ; i++){
			ops[i].setOpaque(false);
			ops[i].setBorder(BorderFactory.createLineBorder(Color.black));
			this.add(ops[i]);
		}
		
		pp = new PlayerPanel(w,h, mc);
		pp.setBounds(0, h-256-TileUtil.MARGIN, w, 256);
		pp.setOpaque(false);
		pp.setBorder(BorderFactory.createLineBorder(Color.black));
		this.add(pp);
	}
	public Game getGame() {
		return game;
	}
	public void setGame(Game game) {
		this.game = game;
	}
	public PlayerPanel getPp() {
		return pp;
	}
	public void setPp(PlayerPanel pp) {
		this.pp = pp;
	}
	public WallPanel getWp() {
		return wp;
	}
	public void setWp(WallPanel wp) {
		this.wp = wp;
	}
	public OpponentPanel[] getOps() {
		return ops;
	}
	public void setOps(OpponentPanel[] ops) {
		this.ops = ops;
	}

}
