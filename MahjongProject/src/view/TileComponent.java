package view;

import Util.TileUtil;
import model.Tile;

/**
 *  Composant définissant une tuile et son image.
 * 
 * @author Martin
 *
 */
@SuppressWarnings("serial")
public class TileComponent extends DraggableImageComponent{
	private Tile tile;
	private char direction;
	private boolean faceVisible;

	public TileComponent(Tile tile, char direction, boolean faceVisible){
		super((faceVisible == true)?tile.getImage():TileUtil.BACK_TILE,true,direction);
		
		this.setTile(tile);
		this.setDirection(direction);
		this.setFaceVisible(faceVisible);
	}

	public Tile getTile() {
		return tile;
	}

	public void setTile(Tile tile) {
		this.tile = tile;
	}

	public char getDirection() {
		return direction;
	}

	public void setDirection(char direction) {
		this.direction = direction;
	}

	public boolean isFaceVisible() {
		return faceVisible;
	}

	public void setFaceVisible(boolean faceVisible) {
		this.faceVisible = faceVisible;
	}

}
