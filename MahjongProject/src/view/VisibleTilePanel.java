package view;

import java.awt.Color;
import java.awt.GridLayout;
import java.util.ArrayList;

import javax.swing.BorderFactory;
import javax.swing.JPanel;

import Util.TileUtil;
import model.Tile;

/**
 * Panel contenant les tuiles visible de chaque joueur. On peut changer sa direction grace à un paramètre pour correspondre à chaque joueur.
 * 
 * @author Martin
 * 
 */
@SuppressWarnings("serial")
public class VisibleTilePanel extends JPanel{

	private char direction;
	private ArrayList<Tile> tiles;
	private int currPanel = 0;
	private JPanel[] g = new JPanel[4];
	
	public VisibleTilePanel(char direction) {
		this.setDirection(direction);
		this.setBorder(BorderFactory.createLineBorder(Color.black));
		
		if(direction == 'N' || direction == 'S') this.setLayout(new GridLayout(4,1));
		else this.setLayout(new GridLayout(1,4));
		
		if(direction == 'W' || direction == 'N') currPanel = 3;
		
		for(int i = 0; i < 4 ; i++) {
			g[i] = new JPanel();
			g[i].setOpaque(false);
			g[i].setLayout(null);
			g[i].setBounds(0, 0, 64, 256);
			this.add(g[i]);
		}
	}
	public char getDirection() {
		return direction;
	}
	public void setDirection(char direction) {
		this.direction = direction;
	}
	public ArrayList<Tile> getTiles() {
		return tiles;
	}
	public void setTiles(ArrayList<Tile> tiles) {
		this.tiles = tiles;
	}

	/**
	 *  Ajoute une combinaison de tuile dans le VisibleTilePanel
	 * 
	 * @param tiles combinaison de 3 tuiles.
	 */
	public void addTile(Tile[] tiles){
		for(int i = 0; i < tiles.length ; i++){
			TileComponent tile = new TileComponent(tiles[i],direction,true);
			int tileSize = TileUtil.tileSizeVTP;
			if(direction == 'S' || direction == 'N')tile.setBounds(i*tileSize, 0, tileSize, tileSize);
			else tile.setBounds(0, i*tileSize, tileSize, tileSize);
			tile.setDraggable(false);
			g[currPanel].add(tile);
		}
		if(direction == 'W' || direction == 'N') currPanel--;
		else currPanel++;
		
		this.repaint();
	}
	
}
