package view;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JPanel;

/**
 * Fenetre du menu principal affichée au démarrage.
 * 
 * @author Martin
 * 
 */
@SuppressWarnings("serial")
public class MenuFrame extends JFrame implements ActionListener{
	JButton createGame = new JButton("Créer partie");
	JButton joinGame = new JButton("Rejoindre partie");
	public MenuFrame() {
		super("MahjongProject");
		this.setLocationRelativeTo(null);
		JPanel main = new JPanel();
		
		createGame.addActionListener(this);
		joinGame.addActionListener(this);
		main.add(createGame);
		main.add(joinGame);
		this.add(main);
		this.setVisible(true);
		this.pack();
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		if(e.getSource() == createGame){
			new ConnectionFrame(0);
		}
		else{
			new ConnectionFrame(1);
		}
		this.dispose();
	}

}
