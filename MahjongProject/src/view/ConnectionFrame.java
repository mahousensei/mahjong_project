package view;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.Box;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextField;

/**
 * Frame de connection. Demande du pseudo, de l'ip et du port.
 * 
 * @author Martin
 *
 */
@SuppressWarnings("serial")
public class ConnectionFrame extends JFrame implements ActionListener{
	private JTextField pseudo = new JTextField("");
	private JLabel pseudoLabel = new JLabel("Pseudo : ");

	private JTextField ip = new JTextField("127.0.0.1:1800");
	private JLabel ipLabel = new JLabel("Ip du serveur : ");

	private JButton button = new JButton("");

	private int mode;

	public ConnectionFrame(int mode) {
		super("MahjongProject");
		this.setLocationRelativeTo(null);
		this.mode = mode;
		
		if(mode == 0){
			pseudo.setText("Player 1");
			button.setText("Créer partie");
			ipLabel.setText("port du serveur : ");
			ip.setText("1800");
		}
		else{
			pseudo.setText("Player X");
			button.setText("Rejoindre partie");
		}

		Box b = Box.createVerticalBox();

		JPanel pseudoPanel = new JPanel();
		pseudo.setColumns(20);
		pseudoPanel.add(pseudoLabel);
		pseudoPanel.add(pseudo);
		b.add(pseudoPanel);

		JPanel ipPanel = new JPanel();

		ip.setColumns(20);
		ipPanel.add(ipLabel);
		ipPanel.add(ip);
		b.add(ipPanel);

		button.addActionListener(this);
		b.add(button);

		this.add(b);
		this.setVisible(true);
		this.pack();
	}

	@Override
	public void actionPerformed(ActionEvent arg0) {
		if(pseudo.getText() != null && pseudo.getText() != ""){
			char type;
			if(mode == 0) type = 'H'; // H pour host
			else type = 'P'; // P pour player
			
			try {
				new LobbyFrame(pseudo.getText(), ip.getText(), type);
			} catch (NumberFormatException e) {
				e.printStackTrace();
			} catch (Exception e) {
				e.printStackTrace();
			}
			
			this.dispose();
		}
		else{
			JOptionPane.showMessageDialog(this, "", "Pseudo non valide", JOptionPane.WARNING_MESSAGE);
		}
	}
}

