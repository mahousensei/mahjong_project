package network;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.Socket;

import Util.TileUtil;
import model.Player;

/**
 *  Classe liant le socket et son joueur.
 * 
 * @author Martin
 *
 */
public class PlayerSocket extends Thread{

    private String pseudo = "";
    private BufferedReader input;
    private PrintWriter output;
    private Player player;
    private Host host;
    private boolean firstPlayer;
    
    /**
     * Construit le PlayerSocket.
     * 
     * @param playerSocket le socket.
     * @param host l'hôte.
     * @throws Exception rien
     */
	public PlayerSocket(Socket playerSocket, Host host) throws Exception {
		if(playerSocket != null){
	        input = new BufferedReader(new InputStreamReader(playerSocket.getInputStream()));
	        output = new PrintWriter(playerSocket.getOutputStream(), true);
	        
	        this.host = host;        
	        
	        pseudo = input.readLine();
	        this.player = new Player(pseudo);
	        this.setFirstPlayer(false);
	        start();
		}
		else{
			pseudo = host.getPseudo();
			this.player = new Player(pseudo);
			this.setFirstPlayer(true);
		}
	}

	/**
	 * Envoit un message au joueur.
	 * 
	 * @param msg le message à envoyer.
	 */
    public void sendMessage(String msg) {
        output.println(msg);
    }

    public String getPseudo() {
		return pseudo;
	}

	public BufferedReader getInput() {
		return input;
	}

	public PrintWriter getOutput() {
		return output;
	}

	public Player getPlayer() {
		return player;
	}

	public void setPlayer(Player player) {
		this.player = player;
	}
	
	public boolean isFirstPlayer() {
		return firstPlayer;
	}

	public void setFirstPlayer(boolean firstPlayer) {
		this.firstPlayer = firstPlayer;
	}

	public Host getHost() {
		return host;
	}

	public void setHost(Host host) {
		this.host = host;
	}

	public void turn(){
		
	}
	
	@Override
    public void run() {
        String line;
        try {
            while (true) {
                line = input.readLine();
                if(line.startsWith("&")){ // tuile défaussée
                	System.out.println("Defausse : " + line);
                	host.getMc().discardTile(TileUtil.stringToTile(line,host.getMc().getDeck()));
                }
                
               // line = input.readLine();
                if(line.startsWith("|")){ // bouton appuyé
                	System.out.println("bouton : " + line);
                	host.getMc().buttonControl(player,Integer.parseInt(line.substring(1)));
                }
                
                if(line.startsWith("!")){ // combinaison choisie
                	System.out.println("combi : " + line);
                	host.getMc().updateAllOpponentPanelVTP(TileUtil.stringToCombination(line.substring(1), host.getMc().getDeck()));
                }
                
                if (line.equals("end")) {
                    break;
                }
            }
        } 
        catch (Exception ex) {
            System.out.println(ex.getMessage());
        }
    } 
} 