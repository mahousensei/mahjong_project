package network;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.Socket;
import java.util.ArrayList;

import model.Tile;
import Util.TileUtil;
import controller.MahjongController;
import view.LobbyFrame;

/**
 * Classe gérant les clients.
 * 
 * @author Martin
 *
 */
public class Client extends Thread{
	private String pseudo;
	private PrintWriter pw;
	private BufferedReader br;
	private Socket client;
	private LobbyFrame lFrame;

	private int idPlayer;
	private int currentPlayerId;
	private ArrayList<String> pseudos = new ArrayList<String>();

	private int nextPlayer = -1;
	private int victoryPlayer = -1;
	private int turnCount = 1;

	private MahjongController mc;

	/**
	 * Construit le client et son socket.
	 * 
	 * @param pseudo le pseudo du client.
	 * @param servername le nom du serveur.
	 * @param port le port du serveur.
	 * @param lFrame la fenêtre du lobby.
	 * @throws Exception rien
	 */
	public Client(String pseudo,String servername, int port, LobbyFrame lFrame) throws Exception{
		this.setPseudo(pseudo);
		this.lFrame = lFrame;
		System.out.println("connection à " + servername + " sur le port " + port);
		client  = new Socket(servername,port);
		br = new BufferedReader( new InputStreamReader(client.getInputStream()) ) ;
		pw = new PrintWriter(client.getOutputStream(),true);
		pw.println(pseudo);

		start(); 
	}

	/**
	 * Construit le lobby
	 * 
	 * @throws IOException rien
	 */
	public void buildLobby() throws IOException{
		String line;
		int nbPlayer = Integer.parseInt(br.readLine());
		this.setIdPlayer(nbPlayer-1);
		for(int i = 1;i < nbPlayer;i++){ // les clients avant
			line = br.readLine();
			pseudos.add(line);
			lFrame.addPlayer(line);
		}

		for(int i = nbPlayer; i <= 4 ; i++){ // lui + les clients apres
			line = br.readLine();
			pseudos.add(line);
			lFrame.addPlayer(line);
		}

		line = br.readLine();
		if(line.equals("dispose lobby"))  lFrame.launchGameClient(this);
	}

	/**
	 * Initialise la partie.
	 * 
	 * @throws IOException rien
	 */
	public void initGame() throws IOException{
		String line = br.readLine(); // id du joueur actuel

		currentPlayerId = Integer.parseInt(line.substring(1)); 
		mc.turnActivator(currentPlayerId, idPlayer, pseudos.get(currentPlayerId));

		line = br.readLine(); // THE WALL
		mc.initWall(TileUtil.stringToArray(line, mc.getDeck()));

		for(int i = 0 ; i < 52 ; i++){
			line = br.readLine(); // animation du mur
			mc.removeTileFromWall(TileUtil.stringToTile(line, mc.getDeck()));
		}

		line = br.readLine(); // THE HAND
		mc.updateHand(TileUtil.stringToArray(line, mc.getDeck()));
	}

	/**
	 * Gestion du tour.
	 * 
	 * @throws IOException rien
	 */
	public void turn() throws IOException{
		mc.displayMessage("Début du tour " + turnCount + " : "  + pseudos.get(currentPlayerId));
		String line;
		Tile lastTile;
		
		if(victoryPlayer == -1){
			if(nextPlayer == -1){ // si tout le mode a passé la dernière tuile.
				line = br.readLine(); // la tuile à retirer du mur.
				Tile t = TileUtil.stringToTile(line, mc.getDeck());
				mc.removeTileFromWall(t);
			}

			if(currentPlayerId == idPlayer ){ // tour du joueur

				if(nextPlayer == -1){
					line = br.readLine(); // la tuile à piocher.
					mc.updateHand(currentPlayerId, idPlayer, TileUtil.stringToTile(line, mc.getDeck())); //14ième tuile
				}
				
				line = br.readLine(); // -1 si personne n'a gagné / 0,1,2,3 si un joueur à gagné.
				victoryPlayer = Integer.parseInt(line);
				if(victoryPlayer != -1) this.victory();


				synchronized(this) { 
					try {
						this.wait(); //on attend que le joueur défausse une tuile
					} catch (InterruptedException e) {
						e.printStackTrace();
					}
				}

				line = br.readLine(); // tuile défaussée
				lastTile = TileUtil.stringToTile(line, mc.getDeck());
				line = br.readLine(); // joueur suivant.
				nextPlayer = Integer.parseInt(line.substring(1));
			}
			else{ // tour d'un autre joueur.
				
				line = br.readLine(); // -1 si personne n'a gagné / 0,1,2,3 si un joueur à gagné.
				victoryPlayer = Integer.parseInt(line);
				if(victoryPlayer != -1) this.victory();
				
				line = br.readLine(); // tuile défaussée
				lastTile = TileUtil.stringToTile(line, mc.getDeck());
				mc.discardTile(lastTile);

				boolean buttons[] = new boolean[3];
				for(int i = 0; i < buttons.length ; i++){
					line = br.readLine(); // active le bouton ou non.
					buttons[i] = line.substring(5).equals("OK");
				}
				mc.buttonActivator(true, buttons);
				TileUtil.Timer(this,5);
				mc.buttonActivator(false, buttons);

				line = br.readLine(); // joueur suivant.
				nextPlayer = Integer.parseInt(line.substring(1));

				if(nextPlayer == idPlayer){
					line = br.readLine(); // nombre de combinaisons possibles.
					int size = Integer.parseInt(line);
					ArrayList<Tile[]> combinations = new ArrayList<Tile[]>();
					for(int i = 0; i < size ; i++){
						line = br.readLine(); // une combinaison.
						combinations.add(TileUtil.stringToCombination(line, mc.getDeck()));
					}
					mc.showDialog(combinations, lastTile);
				}
			}

			if(nextPlayer != -1 && nextPlayer != idPlayer){
				line = br.readLine(); // combinaison à afficher
				mc.updateOpponentPanelVTP(nextPlayer, TileUtil.stringToCombination(line.substring(1), mc.getDeck())); // on met à jour l'OP pour le joueur
			}

			mc.removeLastTile();
			if(nextPlayer != -1){
				currentPlayerId = nextPlayer;
			}
			else{
				mc.addTileToDiscardPanel(lastTile);
				currentPlayerId = ((currentPlayerId + 1) == 4)?0:currentPlayerId+1;
			}

			mc.turnActivator(currentPlayerId, idPlayer, pseudos.get(currentPlayerId));
			turnCount++;
		}
	}

	/**
	 * Gestion de la vitoire d'un joueur.
	 * 
	 * @throws IOException rien
	 */
	public void victory() throws IOException{
		String line = br.readLine();
		ArrayList<Tile> hand = new ArrayList<Tile>(TileUtil.stringToArray(line, mc.getDeck()));
		mc.displayVictory(pseudos.get(victoryPlayer), hand);
	}

	/**
	 * Envoit d'un message à l'hôte.
	 * 
	 * @param msg message à envoyer.
	 */
	public void sendMsg(String msg){
		pw.println(msg);
	}

	public void run() {
		try {
			this.buildLobby();
			this.initGame();
			while(victoryPlayer == -1) {
				this.turn();
			}

		} catch(Exception ex) {
			System.out.println(ex.getMessage());
		}
	}

	public MahjongController getMc() {
		return mc;
	}

	public void setMc(MahjongController mc) {
		this.mc = mc;
	}

	public int getIdPlayer() {
		return idPlayer;
	}

	public void setIdPlayer(int idPlayer) {
		this.idPlayer = idPlayer;
	}

	public int getCurrentPlayerId() {
		return currentPlayerId;
	}

	public void setCurrentPlayerId(int currentPlayerId) {
		this.currentPlayerId = currentPlayerId;
	}

	public String getPseudo() {
		return pseudo;
	}

	public void setPseudo(String pseudo) {
		this.pseudo = pseudo;
	}

	public ArrayList<String> getPseudos() {
		return pseudos;
	}
}
