package network;

import static java.lang.System.out;

import java.net.ServerSocket;
import java.net.Socket;
import java.util.ArrayList;

import controller.MahjongController;
import view.LobbyFrame;

/**
 * Classe gérant l'hôte.
 * 
 * @author Martin
 *
 */
public class Host implements Runnable{

	private ArrayList<String> users = new ArrayList<String>();
	private ArrayList<PlayerSocket> players = new ArrayList<PlayerSocket>();

	private int port;
	private String pseudo;
	private LobbyFrame lFrame;
	
	private MahjongController mc;

	/**
	 * Construit l'Hôte et lance la création du serveur.
	 * 
	 * @param pseudo pseudo
	 * @param port port
	 * @param lFrame lobbyFrame
	 * @throws Exception rien
	 */
	public Host(String pseudo, int port, LobbyFrame lFrame) throws Exception {
		this.pseudo = pseudo;
		users.add(pseudo); // le premier joueur est l'hote lui même
		players.add(new PlayerSocket(null,this)); // si socket null, l'hôte envoit les infos à sa propre interface graphique.
		this.lFrame = lFrame;
		this.port = port;
		new Thread(this).start();
	}

	/**
	 * Créer le serveur : envoit/recoit les bonnes informations pour le lobby.
	 * 
	 * @throws Exception rien
	 */
	public void createserver() throws Exception {
		@SuppressWarnings("resource")
		ServerSocket host = new ServerSocket(port, 3);
		out.println("Now host Is Running on port " + host.getLocalPort());
		while (players.size() < 4) {
			Socket player = host.accept();
			System.out.println("Un joueur de plus !");
			PlayerSocket p = new PlayerSocket(player,this);
			players.add(p);
			users.add(p.getPseudo());
			lFrame.addPlayer(p.getPseudo());

			p.getOutput().println(users.size());// on envoit le nombre de joueur déjà connecté qui corespond à sa position
			
			for(int i = 0; i < users.size()-1;i++){ // on envoit les pseudos deja connecté au nouveau joueur
				p.getOutput().println(users.get(i));
			}
			
			for(PlayerSocket ps : players){
				if(! ps.isFirstPlayer()) ps.getOutput().println(p.getPseudo()); // on envoit le pseudo aux autres joueurs
			}
			
		}
		System.out.println("tous les joueurs sont connectés");
	}
	
	/**
	 * Envoit un message à tous les clients.
	 * 
	 * @param message le message à envoyer.
	 */
	public void sendtoall(String message) {

		for (PlayerSocket ps : players) {
			if (!ps.isFirstPlayer()) {
				ps.sendMessage(message);
			}
		}
	}

	public String getPseudo() {
		return pseudo;
	}

	public void setPseudo(String pseudo) {
		this.pseudo = pseudo;
	}

	public ArrayList<PlayerSocket> getPlayers() {
		return players;
	}

	public void setPlayers(ArrayList<PlayerSocket> players) {
		this.players = players;
	}

	public MahjongController getMc() {
		return mc;
	}

	public void setMc(MahjongController mc) {
		this.mc = mc;
	}

	@Override
	public void run() {
		try {
			this.createserver();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}
