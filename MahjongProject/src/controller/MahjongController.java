package controller;

import java.util.ArrayList;

import javax.swing.ImageIcon;
import javax.swing.JOptionPane;

import network.Client;
import network.Host;
import Util.TileUtil;
import model.Player;
import model.Tile;
import system.Game;
import view.GameFrame;
import view.VictoryFrame;

/**
 * Le controlleur. Gère l'intéraction entre la vue, les modèles et le réseau.
 * 
 * @author Martin
 *
 */
public class MahjongController {
	private Game game = new Game(this); // le jeu logique
	private GameFrame gFrame; // le jeu visuel
	private Host host; // la connection entre les joueurs
	private Client client;
	
	private ArrayList<Tile> deck;
	
	/**
	 * Construit le controller pour les joueurs non-hôte.
	 * 
	 * @param client un joueur non hôte
	 */
	public MahjongController(Client client){
		deck = new ArrayList<Tile>(TileUtil.initDeck());
		this.client = client;
		gFrame = new GameFrame(this);
		gFrame.setTitle(gFrame.getTitle() + " - " + client.getPseudo());
	}
	
	/**
	 * Construit le constructeur de l'hôte.
	 * 
	 * @param host le joueur hôte
	 */
	public MahjongController(Host host){
		deck = new ArrayList<Tile>(TileUtil.initDeck());
		this.host = host;
			Player[] players = new Player[4];
			for(int i = 0; i < players.length ; i++){
				players[i] = host.getPlayers().get(i).getPlayer();
			}
			
		gFrame = new GameFrame(this);
		gFrame.setTitle(gFrame.getTitle() + " - " + host.getPseudo());
		game = new Game(players,this); // on crée la partie en reliant le jeu à la fenêtre pour recevoir les events : boutons + défausse de tuile
	}
	
	/**
	 * Gestion de la tuile défaussée.
	 * 
	 * @param tile la tuile défaussée
	 */
	public void discardTile(Tile tile){	
		if(host != null){
			game.getTable().setLastTile(tile);
			game.getPlayers()[game.getCurrentPlayerId()].getHand().remove(tile);
			
			for(int i = 1 ; i < host.getPlayers().size() ; i++){
				host.getPlayers().get(i).sendMessage("&" + tile.getId());
			}
			
			synchronized(game.getThread()){
				game.getThread().notify();
			}
			
			if(game.getCurrentPlayerId() == 0){
				gFrame.getTp().getPp().getDdp().removeTile(tile);
			}
		}
		else if(client.getIdPlayer() == client.getCurrentPlayerId()){
			client.sendMsg("&" + tile.getId());
			synchronized(client){
				client.notify();
			}
			gFrame.getTp().getPp().getDdp().removeTile(tile);
		}
		gFrame.getTp().getWp().updateLastTile(tile);
	}
	
	/**
	 * Détermination des chi/pon/ron d'un joueur et envoit les permissions aux autres joueurs.
	 * 
	 * @param playerId l'id du joueur qui a appuyé sur les boutons.
	 * @param activate false, s'il faut desactiver tous les boutons.
	 */
	public void allButtonActivator(int playerId, boolean activate){
		if(activate){
			int nextPlayerId = (game.getCurrentPlayerId()+1 < 4)?game.getCurrentPlayerId()+1:0;
			boolean[] buttons = {(playerId == nextPlayerId)?game.getPlayers()[playerId].canChi(game.getTable().getLastTile()):false, 
					game.getPlayers()[playerId].canPon(game.getTable().getLastTile()), 
					game.getPlayers()[playerId].canRon(game.getTable().getLastTile())};
			
			if(playerId == 0){
				buttonActivator(activate, buttons);
			}
			else{
				if(buttons[0]) host.getPlayers().get(playerId).sendMessage("|chi|OK");
				else host.getPlayers().get(playerId).sendMessage("|chi|NO");
				
				if(buttons[1]) host.getPlayers().get(playerId).sendMessage("|pon|OK");
				else host.getPlayers().get(playerId).sendMessage("|pon|NO");
				
				if(buttons[2]) host.getPlayers().get(playerId).sendMessage("|ron|OK");
				else host.getPlayers().get(playerId).sendMessage("|ron|NO");
			}
		}
		else{
			buttonActivator(activate, new boolean[3]);
		}
	}
	
	/**
	 * Gestion des boutons chi, pon et ron.
	 * 
	 * @param activate false, s'il faut desactiver tous les boutons.
	 * @param buttons tableau des boutons à activer.
	 */
	public void buttonActivator(boolean activate, boolean[] buttons){
		if(activate){
			if(buttons[0]) gFrame.getSp().getChi().setEnabled(true);
			if(buttons[1]) gFrame.getSp().getPon().setEnabled(true);
			if(buttons[2]) gFrame.getSp().getRon().setEnabled(true);
		}
		else{
			gFrame.getSp().getChi().setEnabled(false);
			gFrame.getSp().getPon().setEnabled(false);
			gFrame.getSp().getRon().setEnabled(false);
		}
	}
	
	/**
	 * Détermination des combinaisons de 3 tuiles possibles en fonction du bouton appuyé, de la main du joueur et de la dernière tuile défaussée.
	 * Envoit des combinaisons au joueur le + prioritaire.
	 * 
	 * @param playerId Id du joueur le plus prioritaire
	 * @param buttonPushed 0 - chi, 1- pon, 2 - ron
	 */
	public void sendCombinations(int playerId, int buttonPushed){
		ArrayList<Tile> temp = new ArrayList<Tile>(game.getPlayers()[playerId].getHand());
		temp.add(game.getTable().getLastTile());
		ArrayList<Tile[]> combTemp = Player.getCombinations(temp, buttonPushed);
		ArrayList<Tile[]> combinations = new ArrayList<Tile[]>();
		
		
		// on ne garde que les combinaisons qui concernent la dernière tuile
		for(int i = 0; i < combTemp.size() ; i++){
			boolean combOk = false;
			for(int j = 0; j < 3 ; j++){
				if(combTemp.get(i)[j].getId() == game.getTable().getLastTile().getId()) combOk = true;
			}
			if(combOk) combinations.add(combTemp.get(i));
		}
		
		if(playerId == 0) showDialog(combinations, game.getTable().getLastTile()); // si c'est l'hote le plus prioritaire, on affiche directement
		else{
			host.getPlayers().get(playerId).sendMessage("" + combinations.size());
			for(Tile[] tab : combinations){
				host.getPlayers().get(playerId).sendMessage(TileUtil.arrayOfTileToString(tab));
			}
		}
	}
	
	/**
	 * Envoit le joueur suivant à tous les joueurs.
	 * @param nextPlayerId id du joueur suivant.
	 */
	public void sendNextPlayer(int nextPlayerId){
		host.sendtoall("&" + nextPlayerId); // on envoit à tout le monde le futur joueur;
	}
	
	/**
	 * Affiche le dialogue pour choisir une combinaison et renvoit de la combinaison choisie à l'hôte.
	 * 
	 * @param combinations liste des combinaisons possibles
	 * @param lastTile dernière tuile défaussée.
	 */
	public void showDialog(ArrayList<Tile[]> combinations, Tile lastTile){
		// on forme le string à afficher
				String[] tabCombinations = new String[combinations.size()];
				for(int i = 0; i < combinations.size(); i++){
					tabCombinations[i] = (i+1) + ") " + TileUtil.combinationToString(combinations.get(i));
				}
		
		String answer =(String) JOptionPane.showInputDialog(gFrame, 
				"Veuillez selectionner une combinaison de tuile", 
				"TileSelector",
				JOptionPane.PLAIN_MESSAGE, 
				new ImageIcon(lastTile.getImage(),"Dernière tuile"), 
				tabCombinations, null);
		
		// le choix de l'utilisateur
		int choice = 0;
		if(answer != null){
			String answerTemp = "" + answer.charAt(0);
			answerTemp += (answer.charAt(1) != ')')?answer.charAt(1):"";
			choice = Integer.parseInt(answerTemp) - 1;
		}
		
		gFrame.getTp().getPp().getVtp().addTile(combinations.get(choice));
		this.displayMessage("Vous avez fait la combinaison : " + TileUtil.combinationToString(combinations.get(choice)));
		
		gFrame.getTp().getPp().getDdp().removeTile(combinations.get(choice)[0]);
		gFrame.getTp().getPp().getDdp().removeTile(combinations.get(choice)[1]);
		gFrame.getTp().getPp().getDdp().removeTile(combinations.get(choice)[2]);
		
		if(client != null){
			client.sendMsg("!" + TileUtil.arrayOfTileToString(combinations.get(choice))); // on renvoit combinaison à l'host
		}
		else{
			this.updateAllOpponentPanelVTP(combinations.get(choice));// si c'est l'hote qui a pris la tuile, on envoit directement la combinaison à tout le monde.
		}
	}
	
	/**
	 * Met à jour les opponentPanel de chaque joueur
	 * 
	 * @param combination combinaison de 3 tuile à afficher.
	 */
	public void updateAllOpponentPanelVTP( Tile[] combination){
		for(int i = 0; i < combination.length ; i++) {
			combination[i].setLock(true);
		}
		
		
		if(game.getWhoTakeTheLastTile() != 0) updateOpponentPanelVTP(game.getWhoTakeTheLastTile(), combination); // on met à jour l'OP de l'hote
		
		for(int i = 1; i < host.getPlayers().size() ; i++) 
			if( i != game.getWhoTakeTheLastTile()) host.getPlayers().get(i).sendMessage("!" + TileUtil.arrayOfTileToString(combination)); // on envoit aux autres joueurs la combinaison
		
		synchronized(game.getThread()){
			game.getThread().notify();
		}
	}
	
	/**
	 * Selectionne le bon opponentPanel où afficher la combinaison de 3 tuiles.
	 * 
	 * @param nextPlayerId id du joueur suivant
	 * @param combination combinaison de 3 tuiles à afficher.
	 */
	public void updateOpponentPanelVTP(int nextPlayerId, Tile[] combination){
		if(host != null){
			this.displayMessage(game.getPlayers()[nextPlayerId].getPseudo() + " a fait la combinaison : " + TileUtil.combinationToString(combination));
			gFrame.getTp().getOps()[nextPlayerId-1].getVtp().addTile(combination);
		}
		else{
			int id = ((nextPlayerId-1) >= 0)?nextPlayerId-1:client.getIdPlayer()-1;
			this.displayMessage(client.getPseudos().get(nextPlayerId) + " a fait la combinaison : " + TileUtil.combinationToString(combination));
			gFrame.getTp().getOps()[id].getVtp().addTile(combination);
		}
		
	}
	
	/**
	 * Met à jour les discardPanel des adversaires
	 * 
	 * @param playerId id du joueur à qui appartenait la tuile.
	 * @param t tuile à défausser
	 */
	public void updateOpponentPanelDP(int playerId, Tile t){
		int id = ((playerId-1) >= 0)?playerId-1:client.getIdPlayer()-1;
		gFrame.getTp().getOps()[id].addDiscardedTile(t);
	}
	
	/**
	 * Actualisation de toutes les mains.
	 */
	public void updateAllHand(){
		updateHand(game.getPlayers()[0].getHand());
		for(int i = 1; i < host.getPlayers().size() ; i++){
			host.getPlayers().get(i).sendMessage(TileUtil.arrayOfTileToString(game.getPlayers()[i].getHand()));
		}
	}
	
	/**
	 * Actualise la main envoyée.
	 * 
	 * @param hand la main
	 */
	public void updateHand(ArrayList<Tile> hand){
		gFrame.getTp().getPp().getDdp().setTiles(hand);
	}
	
	/**
	 * Ajout d'une tuile à la main d'un joueur.
	 * 
	 * @param currentPlayerId id du joueur courant
	 * @param playerId id du joueur
	 * @param tile tuile à ajouter
	 */
	public void updateHand(int currentPlayerId, int playerId,Tile tile){
		if(currentPlayerId == playerId){
			gFrame.getTp().getPp().getDdp().addTiles(tile);
			this.displayMessage("Vous avez pioché la tuile : " + tile);
		}
		else host.getPlayers().get(currentPlayerId).sendMessage(":" + tile.getId());
	}
	
	/**
	 * Initialisation du mur chez tous les joueurs.
	 */
	public void initAllWall(){
		initWall(game.getTable().getWall());
		host.sendtoall(TileUtil.arrayOfTileToString(game.getTable().getWall()));
	}
	
	/**
	 * initialisation d'un mur.
	 * 
	 * @param wall liste de tuile qui compose le mur.
	 */
	public void initWall(ArrayList<Tile> wall){
		gFrame.getTp().getWp().setWall(wall);
	}
	
	/**
	 * réception des boutons.
	 * 
	 * @param player joueur
	 * @param buttonIndex -1 : aucun , 0 : chi , 1 : pon , 2 ron
	 */
	public void buttonControl(Player player, int buttonIndex){
		if(host != null){
			if(player != null) player.setButtonPressed(buttonIndex);
			else game.getPlayers()[0].setButtonPressed(buttonIndex);
		}
		else{
			client.sendMsg("|" + buttonIndex); // envoit du bouton à l'host
		}
	}
	
	public GameFrame getgFrame() {
		return gFrame;
	}
	public void setgFrame(GameFrame gFrame) {
		this.gFrame = gFrame;
	}
	public Game getGame() {
		return game;
	}
	public void setGame(Game game) {
		this.game = game;
	}

	public ArrayList<Tile> getDeck() {
		return deck;
	}

	public void setDeck(ArrayList<Tile> deck) {
		this.deck = deck;
	}

	/**
	 * Supprime la dernière tuile défaussée
	 */
	public void removeLastTile(){
		gFrame.getTp().getWp().updateLastTile(null);
	}
	
	/**
	 * Supprime une tuile du mur de chaque joueur.
	 * 
	 * @param t la tuile à supprimer.
	 */
	public void removeTileFromAllWall(Tile t){
		removeTileFromWall(t);
		host.sendtoall(":" + t.getId());
	}
	
	/**
	 * Supprime une tuile du mur
	 * 
	 * @param t la tuile à supprimer.
	 */
	public void removeTileFromWall(Tile t){
		gFrame.getTp().getWp().removeTile(t);
	}
	
	/**
	 *  Ajoute une tuile au discardPanel
	 * 
	 * @param t la tuile à défausser
	 */
	public void addTileToDiscardPanel(Tile t){
		if(host != null){
			if(game.getCurrentPlayerId() == 0){
				gFrame.getSp().addDiscardedTile(t);
				this.displayMessage("vous avez défaussé la tuile : " + t);
			}
			else{
				this.updateOpponentPanelDP(game.getCurrentPlayerId(), t);
				this.displayMessage(game.getPlayers()[game.getCurrentPlayerId()].getPseudo() + " a défaussé la tuile : " + t);
			}
		}
		else{
			if(client.getCurrentPlayerId() == client.getIdPlayer()){
				gFrame.getSp().addDiscardedTile(t);
				this.displayMessage("vous avez défaussé la tuile : " + t);
			}
			else{
				this.updateOpponentPanelDP(client.getCurrentPlayerId(), t);
				this.displayMessage(client.getPseudos().get(client.getCurrentPlayerId()) + " a défaussé la tuile : " + t);
			}
		}
	}

	
	/**
	 * Affiche un message dans le chat.
	 * 
	 * @param message à afficher
	 */
	public void displayMessage(String message){
		gFrame.getSp().getChat().append("\n" + message);
	}
	
	/**
	 * Gestion des tours. Active la défausse des tuiles.
	 * 
	 * @param currentIdPlayer id du joueur courant
	 * @param idPlayer id du joueur
	 * @param pseudo pseudo
	 */
	public void turnActivator(int currentIdPlayer, int idPlayer, String pseudo){
		if(currentIdPlayer == idPlayer) gFrame.getTp().getPp().getDdp().enableDiscardTile();
		gFrame.getSp().switchPseudo(pseudo);
		
		if(idPlayer == 0 & game.getTurnCount() == 1){
			host.sendtoall("&" + currentIdPlayer);
		}
	}
	
	/**
	 * Envoit le joueur gagnant aux autres joueurs.
	 * 
	 * @param victoryPlayer -1 si personne n'a gagné / 0,1,2,3 si le joueur corespondant a gagné.
	 */
	public void sendIsVictory(int victoryPlayer){
		host.sendtoall("" + victoryPlayer);
	}
	
	/**
	 * Envoit à tous les joueurs la main gagnante.
	 * 
	 * @param victoryPlayer -1 si personne n'a gagné / 0,1,2,3 si le joueur corespondant a gagné.
	 */
	public void manageVictory(int victoryPlayer){
		this.displayVictory(game.getPlayers()[victoryPlayer].getPseudo(), game.getPlayers()[victoryPlayer].getHand());
		host.sendtoall("" + TileUtil.arrayOfTileToString(game.getPlayers()[victoryPlayer].getHand()));
	}
	
	/**
	 * Affiche la fenêtre de victoire.
	 * 
	 * @param pseudo pseudo du joueur gagnant.
	 * @param hand liste de tuile corespondant à la main du joueur gagnant.
	 */
	public void displayVictory(String pseudo, ArrayList<Tile> hand){
		new VictoryFrame(pseudo, hand);
	}
}
