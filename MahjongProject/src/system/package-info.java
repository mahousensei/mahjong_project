/**
 * Ce package contient toutes les classes système utilisées :
 * <ul>
 * <li>La classe principale <code>MahjongSystem.java</code></li>
 * <li>La classe <code>Game.java</code></li>
 * </ul>
 * 
 * @author Dylan Brossel
 */
package system;