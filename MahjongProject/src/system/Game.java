package system;

import java.util.ArrayList;

import controller.MahjongController;
import model.Player;
import model.Table;
import model.Tile;
import Util.TileUtil;

public class Game implements Runnable{
	private MahjongController mc;

	private Table table = new Table();
	private Player[] players = new Player[4];
	private ArrayList<Tile> deck = new ArrayList<Tile>();
	private int currentPlayerId = -1;
	private int whoTakeTheLastTile = -1;
	private boolean discardedTile = false;

	private int turnCount = 1;
	boolean isVictory = false;

	private Thread thread;

	public Game(MahjongController mc) {
	}

	/**
	 * Méthode de lancement de la partie.
	 * 
	 * @param players tableau des 4 joueurs
	 * @param mc le controlleur
	 */
	public Game(Player[] players, MahjongController mc){
		this.players = players;
		this.mc = mc;

		this.thread = new Thread(this);
		thread.start();
	}

	/**
	 * Si personne ne prend la tuile défaussée, c'est au tour du joueur suivant, sinon, c'est a celui qui a pris la dernière tuile.
	 */
	private void whoseTurn(){
		if(whoTakeTheLastTile == -1){
			currentPlayerId = ((currentPlayerId + 1) == 4)?0:currentPlayerId+1;
		}
		else{
			currentPlayerId = whoTakeTheLastTile;
		}
	}

	/**
	 * Initialisation de la partie.
	 */
	private void init(){
		table.setWall(TileUtil.shuffle(deck));
		currentPlayerId = TileUtil.random(3);
		mc.turnActivator(currentPlayerId, 0, players[currentPlayerId].getPseudo());
		tileDistribution(table.getWall());
	}

	/**
	 * Distibution des tuiles, 14 tuiles sont retirées du mur, elles sont bloquées.
	 * 
	 * @param tiles table.getall()
	 */
	private void tileDistribution(ArrayList<Tile> tiles){
		//Par convention, la premiere tuile du mur est celle la plus au nord-est, en haut. Le reste suit dans le sens horloge. Le premier cote est l'Est.
		int side = TileUtil.random(3); // le cote ou on coupe = 1d4
		int coupure = TileUtil.random(10)+2; // a partir de quel tuile on coupe = 2d6
		int idCurrTile = 34*side + coupure;

		mc.initAllWall();

		//retirer 14 tuiles avant idCurrTile
		for(int i = 1; i <= 14; i++){
			if((idCurrTile-1) >= 0){
				table.getWall().remove(idCurrTile-1);
			}
			else{
				table.getWall().remove(table.getWall().size()-1);
			}
		}


		//4 tuiles par 4 tuiles
		for(int i = 0; i < 3; i++){ // 3 paquet de 4 tuiles
			for(int j = 0; j < 4; j++){ // 4 joueurs
				for(int k = 0 ; k < 4; k++){ // paquet de 4 tuiles
					if(idCurrTile == tiles.size()) idCurrTile = 0;
					players[j].getHand().add(table.getWall().get(idCurrTile));
					mc.removeTileFromAllWall(table.getWall().get(idCurrTile));
					table.getWall().remove(idCurrTile);
				}
			}
		}
		//1 de plus pour chacun pour faire 13
		for(int i = 0; i < 4; i++){
			if(idCurrTile == tiles.size()) idCurrTile = 0;
			players[i].getHand().add(table.getWall().get(idCurrTile));
			mc.removeTileFromAllWall(table.getWall().get(idCurrTile));
			table.getWall().remove(idCurrTile);
		}
		table.setIdCurrTile(idCurrTile);

		mc.updateAllHand();
	}

	/**
	 * 1. On pioche TheLastTile ou dans le mur suivant la valeur de WhoTakeTheLastTile().
	 * 2. Si victoire lancer la méthode Victoire().
	 * 3. Sinon, rejet d'une piece.
	 * 4. Check des combinaisons possibles de la tuile avec toutes les autres mains.
	 * 5. Timer de 5 sec.
	 * 6. Check des boutons appuyés et des priorités ron, pon, chi.
	 * 7. Si quelqu'un a pris la tuile : on modifie whoTakeTheLastTile() et on met la piece dans la bonne main.
	 * 8. Sinon, whoTakeTheLastTile() renvoit null.
	 * 9. Changement du joueur courant.
	 */
	public void turn(){
		mc.displayMessage("Début du tour " + turnCount + " : "  + players[currentPlayerId].getPseudo());

		drawTile();

		if(players[currentPlayerId].isVictory()) mc.sendIsVictory(currentPlayerId);
		else{ 
			mc.sendIsVictory(-1);
			synchronized(this.getThread()) { 
				try {
					this.getThread().wait(); //on attend qu'un joueur défausse une tuile
				} catch (InterruptedException e) {
					e.printStackTrace();
				}
			}
			for(int i=0; i<players.length;i++){
				if(i != currentPlayerId) mc.allButtonActivator(i,true);
			}
			TileUtil.Timer(this.getThread(),5.1); // pendant ce temps, les joueurs cliquent ou pas sur un bouton

			int priority = -1;
			int priorityId = -1;
			for(int i=0; i<players.length;i++){
				if(players[i].getButtonPressed() > priority){
					priorityId = i;
					priority = players[i].getButtonPressed();
				}
				players[i].setButtonPressed(-1);
			}
			whoTakeTheLastTile = priorityId;

			mc.sendNextPlayer(whoTakeTheLastTile);

			if(whoTakeTheLastTile != -1)mc.sendCombinations(whoTakeTheLastTile, priority);
			else{ // jeter la tuile dans la défausse du joueur chez tout le monde.
				players[currentPlayerId].getDiscard().add(table.getLastTile());
				mc.addTileToDiscardPanel(table.getLastTile());
				table.setLastTile(null);
				mc.removeLastTile();
			}

			mc.allButtonActivator(0, false);

			if(whoTakeTheLastTile > 0){
				synchronized(this.getThread()) { //pour attendre que le joueur choisisse la combinaison
					try {
						this.getThread().wait(); //on attend qu'un joueur défausse une tuile
					} catch (InterruptedException e) {
						e.printStackTrace();
					}
				}
			}

			whoseTurn(); 

			turnCount++;
			mc.turnActivator(currentPlayerId, 0, players[currentPlayerId].getPseudo());
		}
	}

	/**
	 * Pioche une tuile dans la défausse ou le mur.
	 */
	public void drawTile(){
		Tile tile;
		if(table.getLastTile() == null || whoTakeTheLastTile == -1){ //si premier tour ou que tout le monde osef de la derniere jetee
			if(table.getIdCurrTile() == table.getWall().size()) table.setIdCurrTile(0);
			tile = table.getWall().get(table.getIdCurrTile());
			mc.removeTileFromAllWall(table.getWall().get(table.getIdCurrTile()));
			table.getWall().remove(table.getIdCurrTile());
			mc.updateHand(currentPlayerId, 0,tile);
		}
		else{
			tile = table.getLastTile();
			table.setLastTile(null);
			mc.removeLastTile();
		}
		players[currentPlayerId].getHand().add(tile);
	}

	/**
	 * Gestion de la victoire d'un joueur.
	 */
	public void victory(){
		mc.manageVictory(currentPlayerId);
	}


	@Override
	public void run(){
		mc.displayMessage("Partie lancée !");
		deck = new ArrayList<Tile>(mc.getDeck());
		init();
		while(! players[currentPlayerId].isVictory()){
			turn();
		}
		victory();
	}
	
	public Table getTable() {
		return table;
	}

	public void setTable(Table table) {
		this.table = table;
	}

	public Player[] getPlayers() {
		return players;
	}

	public Player getPlayer(int i) {
		return players[i];
	}

	public void setPlayers(Player[] players) {
		this.players = players;
	}

	public ArrayList<Tile> getDeck() {
		return deck;
	}

	public void setDeck(ArrayList<Tile> deck) {
		this.deck = deck;
	}

	public int getCurrentPlayerId() {
		return currentPlayerId;
	}

	public void setCurrentPlayerId(int currentPlayerId) {
		this.currentPlayerId = currentPlayerId;
	}

	public int getTurnCount() {
		return turnCount;
	}

	public void setTurnCount(int turnCount) {
		this.turnCount = turnCount;
	}

	public boolean isDiscardedTile() {
		return discardedTile;
	}

	public void setDiscardedTile(boolean discardedTile) {
		this.discardedTile = discardedTile;
	}
	
	public int getWhoTakeTheLastTile() {
		return whoTakeTheLastTile;
	}

	public void setWhoTakeTheLastTile(int whoTakeTheLastTile) {
		this.whoTakeTheLastTile = whoTakeTheLastTile;
	}

	public Thread getThread() {
		return thread;
	}

	public void setThread(Thread thread) {
		this.thread = thread;
	}
}
