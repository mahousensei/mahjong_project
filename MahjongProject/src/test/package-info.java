/**
 * Ce package contient toutes les classes de test :
 * <ul>
 * <li>La classe de test des mains <code>TestPlayer.java</code></li>
 * </ul>
 * 
 * @author Dylan Brossel
 */
package test;